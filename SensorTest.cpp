#include <iostream>
#include <fstream>
#include <sstream>
#include <deque>
#include "SensorTest.h"

using namespace std;

//File of the test data we're reading
string filepath = "TestData\\test_1_i.csv";

//Simulated sensor pins
string pwm, current, millis;

//deque of the last 10 samples
deque<int> samples;
//simple 10 point derivative filter, if first 5 samples are larger than last 5 samples, the output will be negative
int filter[10] = {-1, -1, -1, -1, -1, 1, 1, 1, 1, 1};

int main () {
    //Initialize file stream
    ifstream fp(filepath);
    string line;

    //Main program loop, this will setup global variables to be the simulated sensor pins
    //and then sample them as the arduino would
    while(getline(fp, line)) {

        //Turn the line into a stream and read the tokens that are deliminated by commas
        istringstream lineStream(line);
        getline(lineStream, pwm, ',');
        getline(lineStream, current, ',');
        getline(lineStream, millis, ',');

        //Simulate sampling the sensor, returns true when fault is found, otherwise false
        if(sampleSensor()) {
            cout << "Fault found at " << millis << "!\n\r";
            break;
        }
    }
    cout << "Sensor test complete!\n\r";
}

//simulates reading the analog pin that current sense is on
int analogRead() {
    return stoi(current);
}


int sampleSensor() {
    //Sample the current sense and add it to the pile
    samples.push_back(analogRead());

    //Check if we have enough samples to test, need 10
    if(samples.size() > 10){
        //remove the oldest sample
        samples.pop_front();

        //Use the digital filter to calculate the derivative
        int derivative = 0;
        for(int i = 0; i < samples.size(); i++){
            derivative += samples.at(i) * filter[i];
        }

        //Return true when current drops sharply
        if(derivative < -10){
            return 1;
        }
    }

    //Current is increasing, return false
    return 0;

}

